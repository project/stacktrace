<?php

namespace Drupal\stacktrace\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Solution item annotation object.
 *
 * @see \Drupal\stacktrace\Plugin\SolutionPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
class SolutionPlugin extends Plugin {


  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * The exception class the solution is intended for.
   *
   * @var string
   */
  public $exception;

}
