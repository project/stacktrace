<?php

namespace Drupal\stacktrace\Plugin;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for Solution plugins.
 */
abstract class SolutionPluginBase extends PluginBase implements SolutionPluginInterface {


  // Add common methods and abstract methods for your plugin type here.

}
