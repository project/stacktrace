<?php

namespace Drupal\stacktrace\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Solution plugins.
 */
interface SolutionPluginInterface extends PluginInspectionInterface {

  /**
   * Get the description of the solution.
   *
   * @return string
   *   The solution to the error.
   */
  public function getDescription();

  /**
   * Provide a link to some documentation for the exception type.
   *
   * @return string
   *   A link to some documentation or other helpful resource.
   */
  public function getLink();

}
