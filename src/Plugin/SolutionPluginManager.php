<?php

namespace Drupal\stacktrace\Plugin;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides the Solution plugin manager.
 */
class SolutionPluginManager extends DefaultPluginManager {


  /**
   * Constructs a new SolutionPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/SolutionPlugin', $namespaces, $module_handler, 'Drupal\stacktrace\Plugin\SolutionPluginInterface', 'Drupal\stacktrace\Annotation\SolutionPlugin');

    $this->alterInfo('stacktrace_solution_plugin_info');
    $this->setCacheBackend($cache_backend, 'stacktrace_solution_plugin_plugins');
  }

}
